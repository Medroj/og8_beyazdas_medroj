package kontrollstrukturenAB1;

import java.util.Scanner;

public class BMI {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie bitte ihr Körpergewicht in kg ein: ");
		double kg = scan.nextDouble();
		System.out.println("Geben Sie ihre Körpergröße in cm ein: ");
		double gross = scan.nextDouble();
		System.out.println("Geben Sie an ob sie M(1) oder W(0) sind");
		int geschlecht = scan.nextInt();

		double bmi = (kg / ((gross / 100) * (gross / 100)));
		if (geschlecht <= 0) {
			if (bmi < 19) {
				System.out.println("Sie sind Untergewichtig");
			}
			if (bmi >= 19 && bmi <= 24) {
				System.out.println("Sie haben Normalgewicht");
			}
			if (bmi > 24) {
				System.out.println("Sie sind Übergewichtig");
			}
		}
		if (geschlecht >= 1) {
			if (bmi < 20) {
				System.out.println("Sie sind Untergewichtig");
			}
			if (bmi >= 20 && bmi <= 25) {
				System.out.println("Sie haben Normalgewicht");
			}
			if (bmi > 25) {
				System.out.println("Sie sind Übergewichtig");
			}
		}
		System.out.println("BMI: " + bmi);
	}

}
