package addon;

public class Addon {

	private double preis;

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBestand() {
		return bestand;
	}

	public void setBestand(int bestand) {
		this.bestand = bestand;
	}

	public int getMaxbestand() {
		return maxbestand;
	}

	public void setMaxbestand(int maxbestand) {
		this.maxbestand = maxbestand;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private String name;

	private int bestand;

	private int maxbestand;

	private int id;

	public Addon(double preis, String name, int bestand, int maxbestand, int id) {
		this.preis = preis;
		this.name = name;
		this.bestand = bestand;
		this.maxbestand = maxbestand;
		this.id = id;
	}

	public void neubestand(int bestand) {
		this.bestand = bestand;
	}

	public double gesamtwert(double preis) {

		return preis;
	}

}
