package dateinVerwaltung;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;





public class DateinVerwaltung {

	private File file;
	public DateinVerwaltung(File file) {
		this.file = file;
	}
	public void schreibe(String s) {
		try {
			FileWriter fw = new FileWriter(this.file,true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.newLine();
			bw.write(s);
			bw.flush();
			bw.close();
		}
		catch(IOException e) {
		System.out.println("File wahrscheinlich nicht vorhanden");
		e.printStackTrace();
		}
	}
	public void lesen() {
		try {
			FileReader fr = new FileReader(this.file);
			BufferedReader br = new BufferedReader(fr);
			String s;
			while((s = br.readLine()) != null) {
				System.out.println(s);
			}
		}
			catch(Exception e) {
				System.out.println("File wahrscheinlich nicht vorhanden");
				e.printStackTrace();
			}
		}
	
	
	public static void main(String[] args) {
		File file = new File("OG8.txt");
		DateinVerwaltung dv = new DateinVerwaltung(file);
		int[] prim = new int[1_000_000];
		int test = 1_000_000;
		Primzahlmio.fullprim(prim , test);
		for(int i = 0; i<prim.length; i++) {
			System.out.println(prim[i]);
		}
            dv.schreibe("name");
	}

}
