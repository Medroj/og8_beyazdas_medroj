package rekursion;

public class Qsum {

	public static int qsum(int x){
		if(x>1)return x*x + qsum(x-1);		
		else return 1;
		
	}
}
