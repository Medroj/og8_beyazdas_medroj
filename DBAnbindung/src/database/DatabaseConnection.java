package database;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;

public class DatabaseConnection {
	private String driver = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost/keks";
	private String user = "root";
	private String password = "";

	public boolean insertKekse(String bezeichnung, String sorte) {

		String sql = "Insert into t_kekse ( bezeichnung, sorte ) Values ('"+bezeichnung +" ' , '"+ sorte +"');";

		try {
			// JDBC - Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);

			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			con.close();
		} catch (Exception e) {

			e.printStackTrace();
			return false;
		}

		return true;
        
	}

	public static void main(String[] args) {
		DatabaseConnection dbc = new DatabaseConnection();
		dbc.insertKekse("---", "---");
	}
}
