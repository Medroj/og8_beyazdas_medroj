package keyStore;

public class KeyStore01 {
	private String[] keyStore;

	public KeyStore01() {
		this.keyStore = new String[100];
	}

	public KeyStore01(int lenght) {
		this.keyStore = new String[lenght];
	}

	public boolean add(String eintrag) {
		for (int i = 0; i < this.keyStore.length; i++) {
			if (this.keyStore[i] == null) {
				this.keyStore[i] = eintrag;
				return true;
			}

		}
		return false;
	}

	public void clear() {
		for (int i = 0; i < this.keyStore.length; i++) {
			keyStore[i] = null;
		}
	}

	public int indexOf(String eintrag) {
		for (int i = 0; i < this.keyStore.length; i++) {
			if (keyStore[i].equals(eintrag)) {
				return i;
			}
		}
		return -1;
	}

	public boolean remove(String eintrag) {
		for (int i = 0; i < this.keyStore.length; i++) {
			if (this.keyStore[i].equals(eintrag)) {
				this.keyStore = null;
				return true;
			}

		}
		return false;
	}

	public boolean remove(int eintrag) {
		keyStore[eintrag] = null;
		if (keyStore[eintrag] == null) {
			return true;
		} else {
			return false;
		}

	}

	public int size() {
		int count = 0;
		for (int i = 0; i < this.keyStore.length; i++) {
			if (keyStore[i] == null) {
				count++;
			}
		}
		int length = this.keyStore.length - count;
		if (length > Integer.MAX_VALUE) {
			return Integer.MAX_VALUE;
		}
		return length;
	}

	public String toString() {
		String show = "";
		for (int i = 0; i < this.keyStore.length; i++) {
			show += this.keyStore[i] + ", ";

		}
		return show;
	}
}
