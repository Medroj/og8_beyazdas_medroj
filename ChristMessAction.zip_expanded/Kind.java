import java.util.*;

public class Kind implements Comparable {
	// Jedes Kind hat einen Nachnamen, Vornamen, Geburtstag, Wohnort und
	// Bravheitsgrad
	// Erstellen Sie einen vollparametrisierten Konstruktor, Getter/Setter und eine
	// toString-Methode
	public String vorname, nachname, geburtsdatum, bravheitsgrad, ort;

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getGeburtsdatum() {
		return geburtsdatum;
	}

	public void setGeburtsdatum(String geburtsdatum) {
		this.geburtsdatum = geburtsdatum;
	}

	public String getBravheitsgrad() {
		return bravheitsgrad;
	}

	public void setBravheitsgrad(String bravheitsdgrad) {
		this.bravheitsgrad = bravheitsdgrad;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public Kind(String vorname, String nachname, String geburtsdatum, String bravheitsdgrad, String ort) {
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtsdatum = geburtsdatum;
		this.bravheitsgrad = bravheitsdgrad;
		this.ort = ort;
	}

	public Kind() {
		super();
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		if (this.bravheitsgrad.compareTo(((Kind) o).getBravheitsgrad()) != 0) {
			return this.bravheitsgrad.compareTo(((Kind) o).getBravheitsgrad());
		} else {
			return this.ort.compareTo(((Kind) o).getOrt());
		}
	}

	@Override
	public String toString() {
		return "Kind [vorname=" + vorname + ", nachname=" + nachname + ", geburtsdatum=" + geburtsdatum
				+ ", bravheitsgrad=" + bravheitsgrad + ", ort=" + ort + "]";
	}

	
}
