import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class KindGenerator {

	// Hier eine Methode erstellen, um die Datei der Kinder auszulesen und mit den
	// Daten eine Liste an Kind-Objekte anlegen
	public static ArrayList<Kind> generateKinderList() {
		File file = new File("kinddaten.txt");
		int counter = 0;
		ArrayList<Kind> kind = new ArrayList<Kind>(10_000);
		// Kind[] kind = new Kind[10_000];

		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String s = "";
			while ((s = br.readLine()) != null) {
				Kind kinder = new Kind();
				String neuezeile = s.replaceAll(",", "");
				String[] part = neuezeile.split(" ");
				kinder.vorname = part[0];
				kinder.nachname = part[1];
				kinder.geburtsdatum = part[2];
				kinder.bravheitsgrad = part[3];
				String ort = "";

				for (int i = 4; i < part.length; i++) {
					ort += part[i] + " ";
					kinder.setOrt(ort);
				}
				kind.add(kinder);
				// kind[counter] = kinder;
				// counter++;

			}
			Collections.sort(kind);
		} catch (Exception e) {
			System.out.println("File wahrscheinlich nicht vorhanden");
			e.printStackTrace();
		}
		return kind;
	}
}
