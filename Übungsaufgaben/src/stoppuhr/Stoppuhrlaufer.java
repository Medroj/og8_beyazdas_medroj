package stoppuhr;


public class Stoppuhrlaufer {

	private long startzeit;
	private long endzeit;

	public long getStartzeit() {
		return startzeit;
	}

	public void setStartzeit(long startzeit) {
		this.startzeit = startzeit;
	}

	public long getEndzeit() {
		return endzeit;
	}

	public void setEndzeit(long endzeit) {
		this.endzeit = endzeit;
	}

	public void start() {
		this.startzeit = System.currentTimeMillis();
	}

	public void stop() {
		this.endzeit = System.currentTimeMillis();
	}

	public long getDauerInMs() {
		long dauer = endzeit - startzeit;
		return dauer;
	}

	public void reset() {
		this.startzeit = 0;
		this.endzeit = 0;
	}

}