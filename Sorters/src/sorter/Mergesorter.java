package sorter;

import java.util.Arrays;

public class Mergesorter {

	public static void mergeSort(int[] a, int n) {
        if (n < 2)
            return;
        System.out.println(Arrays.toString(a));
        int div = n / 2;
        int[] l = new int[div];
        int[] r = new int[n - div];

        for (int i = 0; i < div; i++) {
            l[i] = a[i];
        }
        for (int i = div; i < n; i++) {
            r[i - div] = a[i];
        }
        mergeSort(l, div);
        mergeSort(r, n - div);

        merge(a, l, r, div, n - div);
        System.out.println("");
    }

	public static void merge(int[] a, int[] l, int[] r, int left, int right) {

        int i = 0, j = 0, k = 0;

        while (i < left && j < right) {

            if (l[i] <= r[j])
                a[k++] = l[i++];
            else
                a[k++] = r[j++];

        }

        while (i < left)
            a[k++] = l[i++];

        while (j < right)
            a[k++] = r[j++];
       // System.out.println("");
    }


	public static void main(String[] args) {
		// TODO Auto-generated method stub

		 int[] a = { 5, 1, 6, 2, 3, 4 ,9 ,7 ,8, 10 , 14, 16, 12, 18, 11, 13, 15, 17 ,20 ,19 };
	        
	     
	 
	        System.out.println("Unstortierte Liste: ");
	        System.out.println("");
	        for (int i = 0; i < a.length; i++) {
	        
	            System.out.print(a[i] +  " ");
	        }
	        System.out.println();
	        System.out.println("");
	        mergeSort(a, a.length);
	        System.out.println("Sortierte Liste");
	        System.out.println("");
	        for (int i = 0; i < a.length; i++)
	          
	            System.out.print(a[i] + " ");
	}

}
